'''
Data Preprocessing for RadialGAN (Jinsung Yoon 09/10/2018)

Inputs:
  - No: No of samples
  - Dim: No of features
  - Set_No: Number of groups
  
Outputs:
  - Train_X, Test_X: Features
  - Train_G, Test_G: Groups
  - Train_Y, Test_Y: Labels
  - Train_M, Test_M: Masks (Where features are observable)
'''
import numpy as np

def Data_Preprocess(No = 1000, Dim = 10, Set_No = 3, mask_rate = 0.5):
    
    #%% 1. Feature Generations
    
    # For each group
    for k in range(Set_No):
        # Mean and Covariance differences among each group
        mean_val = np.random.uniform(-1., 1., size = [Dim])
        cov_val_temp = np.random.uniform(-1., 1., size = [Dim, Dim])
        cov_val = 0.5 * (cov_val_temp + np.transpose(cov_val_temp)) 
        
        Temp_Data = np.random.multivariate_normal(mean_val,cov_val,No)
        
        if k == 0:
            
            Data = Temp_Data          
            Group = np.ones([No,1])      
            
        if k > 0:
            Data = np.concatenate([Data,Temp_Data], axis=0)
            
            Temp_Group = np.ones([No,1]) * (k+1)
            Group = np.concatenate([Group,Temp_Group], axis=0)
            
    # Label generation (Shared)
    y_coef = np.random.uniform(-1., 1., size = [Dim])
    
    Label = np.zeros([No*Set_No,])
    for i in range(No*Set_No):
        temp = np.dot(y_coef, Data[i,:]) + 0.1*np.random.normal(0,1)
        temp_label = 1/(1+np.exp(-temp))
        Label[i] = np.random.binomial(1,temp_label)        
    
    #%% Feature set selection (Which features are selected for each group)
    FSet = np.zeros([Set_No,Dim])
    for k in range(Set_No):
        Temp = np.random.uniform(0., 1., size = [Dim])
        FSet[k,:] = Temp < mask_rate
        
    # Data regenerating
    for k in range(Set_No):
        idx = np.where(Group == (k+1))
        for j in range(Dim):
            if (FSet[k,j] == 0):
                Data[idx[0], j] = 0
    
    # Mask generation
    Mask = np.zeros([No*Set_No,Dim])
    for k in range(Set_No):
        idx = np.where(Group == (k+1))
        for j in range(Dim):
            if (FSet[k,j] == 1):
                Mask[idx[0], j] = 1    
    
   
   #%% Train Test Division    
    Train_No = int(4*No*Set_No/5)
    
    idx = np.random.permutation(No*Set_No)
    train_idx = idx[:Train_No]
    test_idx = idx[Train_No:(No*Set_No)]
    
    Train_X = Data[train_idx,:]
    Test_X = Data[test_idx,:]

    Train_M = Mask[train_idx,:]
    Test_M = Mask[test_idx,:]
    
    Train_Y = Label[train_idx]
    Test_Y = Label[test_idx]
    
    Train_G = Group[train_idx]
    Test_G = Group[test_idx]

    return [Train_X, Train_M, Train_G, Train_Y, Test_X, Test_M, Test_G, Test_Y, FSet]
  